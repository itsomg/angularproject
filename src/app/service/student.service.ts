import { Injectable } from '@angular/core';
import { studentDetailsElement } from '../interfaces/studentDetailsElement';

@Injectable({
  providedIn: 'root'
})
export class StudentService {
  public studentList: studentDetailsElement[] = [];
  public iterator: number = 1;
  // we can also behaviorsubject here but due to time limit can just take aaray here 
  constructor() { }
}

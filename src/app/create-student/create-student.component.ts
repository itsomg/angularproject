import { studentDetailsElement } from './../interfaces/studentDetailsElement';
import { StudentService } from './../service/student.service';
import { Component, OnInit } from '@angular/core';
import { from, Observable } from 'rxjs';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { map, startWith } from 'rxjs/operators';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-create-student',
  templateUrl: './create-student.component.html',
  styleUrls: ['./create-student.component.scss']
})

export class CreateStudentComponent implements OnInit {
  public division: any[] = [
    { value: 'A' },
    { value: 'B' },
  ];

  public class: any[] = [
    { value: '1st' },
    { value: '2nd' },
    { value: '3rd' },
    { value: '4th' },
    { value: '5th ' },
  ];

  public options: string[] = ['Pune', 'Nashik', 'Baramati'];

  //studentForm
  public studentForm: FormGroup = this.fb.group({
    id: [''],
    firstName: [''],
    lastName: [''],
    class: [''],
    gender: [''],
    division: [''],
    address: [''],
    city: ['']
  });
  public filteredOptions: Observable<string[]> | undefined;

  /**
   * constructor
   * @param fb FormBuilder
   * @param service StudentService
   * @param router Router
   */
  constructor(
    private fb: FormBuilder,
    private service: StudentService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.service.iterator = this.service.iterator;
    const id: string = activatedRoute.snapshot.params.id;
    if (id) {
      this.setStudentValue(id);
    }
  }

  /**
   * ngOnInit
   */
  ngOnInit(): void {
    this.filteredOptions = this.studentForm.controls.city.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  /**
   * _filter
   * @param value searc value
   * @returns filterd value
   */
  private _filter(value: string): string[] {
    const filterValue = value;
    return this.options.filter(option => option.indexOf(filterValue) === 0);
  }

  /**
   *
   * @param {*} id student id
   */
  setStudentValue(id: any) {
    let editData = this.service.studentList[id];
    this.studentForm.patchValue({
      id: editData.id,
      firstName: editData.firstName,
      lastName: editData.lastName,
      class: editData.class,
      gender: editData.gender,
      division: editData.division,
      address: editData.address,
      city: editData.city,
    });
  }

  /**
   * submit
   * @param id student id
   */
  submit(id: any) {
    if (id) {
      console.log("test")
      const elementsIndex = this.service.studentList.findIndex(element => element.id == id);
      const url = this.service.studentList[elementsIndex];
      url.firstName = this.studentForm.get('firstName')?.value;
      url.lastName = this.studentForm.get('lastName')?.value;
      url.id = this.studentForm.get('id')?.value;
      url.class = this.studentForm.get('class')?.value;
      url.division = this.studentForm.get('division')?.value;
      url.address = this.studentForm.get('address')?.value;
      url.gender = this.studentForm.get('gender')?.value;
      url.city = this.studentForm.get('city')?.value;
      this.router.navigate([''])

    } else {
      this.studentForm.patchValue({ id: this.service.iterator++ });
      this.service.studentList.push(this.studentForm.value);
      this.studentForm.reset();
      this.router.navigate([''])
    }

  }
}

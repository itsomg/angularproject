import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StudentService } from '../service/student.service';


@Component({
  selector: 'app-students-data',
  templateUrl: './students-data.component.html',
  styleUrls: ['./students-data.component.scss']
})
export class StudentsDataComponent implements OnInit {

  displayedColumns: string[] = ['id', 'firstName', 'lastName', 'class', 'gender', 'division', 'address', 'city', 'action']
  public dataSource: any;

  constructor(
    private service: StudentService,
    private ref: ChangeDetectorRef,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
    this.dataSource = this.service.studentList
  }

  /**
   *ngOnInit
   * @memberof StudentsDataComponent
   */
  ngOnInit(): void {
  }

  /**
   *actionEdit
   * @param {number} id item
   * @memberof StudentsDataComponent
   */
  actionEdit(id: number) {
    this.router.navigate(['edit-student', id], { relativeTo: this.activatedRoute })
  }

  /**
   * actionDelete
   * @param {number} id item
   */
  actionDelete(id: number) {
    this.service.studentList.splice(id, 1);
    this.dataSource = '';
    this.ref.detectChanges();
    this.dataSource = this.service.studentList;
  }
  
}

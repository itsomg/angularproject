export interface studentDetailsElement {
    id: number
    firstName: string;
    lastName: string;
    class: string;
    gender: string;
    division: string,
    address: string,
    city: string,
    hobbies: []
  }
import { CreateStudentComponent } from './create-student/create-student.component';
import { StudentsDataComponent } from './students-data/students-data.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', component: StudentsDataComponent },
  { path: 'add-student', component: CreateStudentComponent },
  { path: 'edit-student/:id', component: CreateStudentComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
